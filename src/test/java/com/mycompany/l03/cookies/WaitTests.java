package com.mycompany.l03.cookies;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.time.Duration;
import java.time.LocalTime;

public class WaitTests extends TestBase {

    static Loader loader = new Loader();

    public static final String USERNAME = loader.getLogin();
    public static final String PASSWORD = loader.getPassword();


    @Test
    public void explicitWait() {
        var timeBefore = LocalTime.now();
        //явное (непосредственное, прямое) ожидание
        Wait<WebDriver> wait = new WebDriverWait(driver, 0);

        WebElement userName = driver.findElement(By.id("LOGINUSERNAME"));
        userName.clear();
        userName.sendKeys(USERNAME);

        WebElement password = driver.findElement(By.id("LOGINPASSWORD"));
        password.clear();
        password.sendKeys(PASSWORD);

        driver.findElement(By.id("LOGINENTERBUTTON")).click();


        driver.findElement(By.cssSelector(".Suggesters-Input")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".Suggesters-Input")));
        var timeAfter = LocalTime.now();

        var duration = Duration.between(timeBefore, timeAfter).toSeconds();
        System.out.println("Explicit wait duration: " + duration);

    }
}
